# Lenguas App #

## View this project at [https://adonozo.bitbucket.io/](https://adonozo.bitbucket.io/)

This project is largely the result of a joint effort by the [Iberoamerican Organization States](https://oei.int/oficinas/bolivia). Multiple teams contributed to this project including linguists, graphic designers, sound engineers, and developers. My contribution was the development of this Android application; but I also maintained the underlying service that makes this app work.

## Native Android ##
Some benefits of creating a native Android app was the small package size. small memory usage, and having access to peripherals. The app is available at the [Play Store](https://play.google.com/store/apps/details?id=bo.oei.lenguas.lenguas), but the server is no longer maintained so it may have a very limited functionality.

The app follows the [Activity Lifecycle](https://developer.android.com/guide/components/activities/activity-lifecycle) and the [Fragment Lifecycle](https://developer.android.com/guide/fragments/lifecycle). Besides, it uses libraries from the Android ecosystem which work nicely together. These includes:
  -  Retrofit
  -  RxJava
  -  Gson
  -  Room
  -  Dagger
